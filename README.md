# Start-Stop-Dancer-Daemon

Simple scripts to start and stop Dancer/Dancer2 via command line.

1. copy scripts into your dancer project
2. write the port number into a config in the environment folder
3. adjust the settings:
	start.sh:
		CURRENT=`pwd`
		BIN="${CURRENT}/bin"
		SCRIPTS="${CURRENT}/scripts"
		ENVIRONMENT='development_daemon'
		WORKERS=2
	stop.sh:
		CURRENT=`pwd`
		SCRIPTS="${CURRENT}/scripts"
		ENVIRONMENT='development_daemon'
		WORKERS=2
4. make start and stop axecuteable
5. run "./script/start.sh" to start and "./script/start.sh" to stop