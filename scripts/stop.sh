#!/bin/bash

CURRENT=`pwd`
SCRIPTS="${CURRENT}/scripts"
ENVIRONMENT='development_daemon'
WORKERS=2

# include parse_yaml function
. $SCRIPTS/parse_yaml.sh
# read yaml file
eval $(parse_yaml ./environments/$ENVIRONMENT.yml "config_")

echo "Stop..."
fuser -k -n tcp $config_port
echo "Stoped..."
