#!/bin/bash

CURRENT=`pwd`
BIN="${CURRENT}/bin"
SCRIPTS="${CURRENT}/scripts"
ENVIRONMENT='development_daemon'
WORKERS=2

# include parse_yaml function
. $SCRIPTS/parse_yaml.sh
# read yaml file
eval $(parse_yaml ./environments/$ENVIRONMENT.yml "config_")

echo "Start with...:"
echo "plackup -D -s Starman -p $config_port -E $ENVIRONMENT --workers=$WORKERS -a $BIN/app.psgi"
plackup -D -s Starman -p $config_port -E $ENVIRONMENT --workers=$WORKERS -a $BIN/app.psgi
echo "Started..."